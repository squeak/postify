
module.exports = {

  title: "API",
  type: "api",

  moduleName: "postify",

  documentation: {

    methods: [
      {
        name: "postify",
        path: "scripts/index",
      },
      {
        name: "postified",
        path: "scripts/methods",
        matcher: false,
        methodsAutofill: "all",
      },
    ],
  },

};
