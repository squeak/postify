
Edit a database entry with nice graphical inputs.  

Postify does two things:
  - create a window with a list of inputs based on the configurations passed,
  - keep track of references to attachments in the entry, if an attachment is not referred to anywhere, it is removed when saving.
