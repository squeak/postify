var postify = require("../../../scripts");
var uify = require("uify");

window.demoStart = function (utilities) {

  utilities.section({
    title: "postify",
    demo: function ($container) {

      //
      //                              LIST OF ENTRIES

      var entriesList = [
        {
          title: "Anna Karenina",
          author: "Tolstoï",
          readingDates: "2084-12#2085-03",
          how: "read",
        },
        {
          title: "Crime and Punishment",
          author: "Dostoevsky",
          readingDates: "1241-01#1242-09",
          how: "read",
        },
      ];

      //
      //                              ADD ENTRY BUTTON

      uify.button({
        $container: $container,
        icomoon: "plus",
        css: { fontSize: "2em", },
        click: function () {
          displayPostifyWindow({}, entriesList + 1);
        },
      });

      //
      //                              DISPLAY LIST

      var listOfEntries;
      function makeList () {

        if (listOfEntries) listOfEntries.remove();

        listOfEntries = uify.list({
          $container: $container,
          items: _.map(entriesList, function (entry, index) {
            return {
              label: entry.author,
              title: entry.title,
              text: entry.readingDates,
              comment: entry.how,
              button: {
                icomoon: "quill",
                click: function () {
                  displayPostifyWindow(entry, index);
                },
              },
            };
          }),
        });

      };

      makeList();

      //
      //                              SHOW POSTIFY WINDOW

      function displayPostifyWindow (entry, index) {

        postify({
          entry: entry,
          title: "editing a book",
          structure: [
            {
              name: "title",
              type: "normal",
            },
            {
              name: "author",
              type: "normal",
            },
            {
              name: "readingDates",
              type: "dateRange",
            },
            {
              name: "how",
              type: "radio",
              choices: [ "read", "listened", ],
            },
          ],
          save: function (entryNewValue, done) {
            if (entriesList[index]) entriesList[index] = entryNewValue;
            else entriesList.push(entryNewValue);
            makeList();
            done();
          },
        });

      };

      //                              ¬
      //

    },
  });

};
