# Usage

Load and use postify library with `postify(options)`.  

Or in more details:
```javascript
postify({
  $container: $("body"),
  entry: {
    title: "Anna Karenina",
    author: "Tolstoï",
    readingDates: "2084-12#2085-03",
    how: "read",
  },
  title: "title",
  structure: [
    {
      name: "title",
      type: "normal",
    },
    {
      name: "author",
      type: "normal",
    },
    {
      name: "readingDates",
      type: "date",
    },
    {
      name: "how",
      type: "radio",
      choices: [ "read", "listened", ],
    },
  ],
  // ... check out the whole list of options in API page
});
```
