var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/object");
var uify = require("uify");
require("uify/extension/differences");
var dialogify = require("dialogify");
var inputify = require("inputify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CHANGES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function checkChanges (postified) {

  // if entry is new, consider there are always changes
  if (postified.options.isNewEntry) postified.hasChanges = true
  // check if there are some changes
  else if (!(_.isUndefined(postified.entry) || _.isUndefined(postified.resultEntry))) postified.hasChanges = JSON.stringify($$.object.sort(postified.entry)) === JSON.stringify($$.object.sort(postified.resultEntry)) ? false : true
  else postified.hasChanges = false;

  // make display
  if (postified.hasChanges) postified.windified.titlebar.$el.addClass("modified")
  else postified.windified.titlebar.$el.removeClass("modified");

  // return status
  return postified.hasChanges;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DEFINE VALUE IN OBJECT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: define the value of the object displayed in this postified window
    ARGUMENTS: ( result <object> )
    RETURN: <void>
  */
  defineValueInObject: function (result) {
    var postified = this;
    postified.resultEntry = result;
    checkChanges(postified);
    if (postified.options.changed) postified.options.changed.call(postified, postified.resultEntry);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SAVE ENTRY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: prompt to save entry, doing prior appropriate checks
    ARGUMENTS: ( ø )
    RETURN: <false|undefined> «
      will return false if window shouldn't be saved because something's wrong,
      otherwise undefined
    »
  */
  saveEntryPrompt: function () {
    var postified = this;

    function beforeSaveCallbackAndSave (doNotCloseDialog) {
      if (postified.options.beforeSave) postified.options.beforeSave.call(
        postified,
        postified.resultEntry,
        postified.entry,
        function () { saveEntry(doNotCloseDialog); }
      )
      else saveEntry(doNotCloseDialog);
    };

    function saveEntry (doNotCloseDialog) {

      //
      //                              remove orphan attachments

      if (postified.resultEntry._attachments && _.keys(postified.resultEntry._attachments).length) {

        var entryAsJson = _.clone(postified.resultEntry);
        delete entryAsJson._attachments;
        entryAsJson = JSON.stringify(entryAsJson);
        _.each(postified.resultEntry._attachments, function (attachment, attachmentId) {
          // delete any attachment that is not references anywhere else in the entry
          if (!entryAsJson.match(attachmentId)) {
            delete postified.resultEntry._attachments[attachmentId];
            $$.log.info("Postify automaticall removed orphan attachment "+ attachmentId +" in entry "+ postified.resultEntry._id +".");
          };
        });

      };

      //
      //                              run save function

      postified.options.save(postified.resultEntry, function (savedEntry) {
        postified.hasChanges = false; // set changed status to false
        // close dialog
        if (!doNotCloseDialog) postified.closeWithoutPrompt()
        // keep dialog open and set saved entry
        else {
          // well, if it saved, it's not new anymore, nope?
          if (postified.options.isNewEntry) postified.options.isNewEntry = false;
          // set saved entry as new entry value
          postified.entry = postified.resultEntry = savedEntry;
          postified.inputified.set(savedEntry);
          postified.recalculateOptions();
          postified.windified.refreshDisplay({ entry: savedEntry, });
        };
      });

      //                              ¬
      //

    };

    // no changes (or only invalid changes)
    if (!postified.hasChanges) {
      if (postified.$window.find(".invalid").length) uify.alert.warning("There is no valid change to save.\nSome inputs have changes but their values are invalid, please modify them if you want to save.")
      else uify.alert("No modifications to save.");
      return false;
    }
    // missing some mandatory values
    else if (postified.$window.find(".mandatory_missing").length) {
      uify.alert.error("Some mandatory inputs haven't been properly defined.")
      return false
    }
    // there are some invalid changes
    else if (postified.$window.find(".invalid").length && !confirm("Some inputs have invalid values, if you continue, those values will be ignored and lost.")) return false
    // good (or forced) to save
    else uify.confirm({
      content: "Save modifications to database?",
      buttons: [
        "cancel",
        {
          title: "save",
          key: "shift + enter",
          click: function (e) { beforeSaveCallbackAndSave(true); },
        },
        {
          title: "save and close",
          key: "enter",
          click: function (e) { beforeSaveCallbackAndSave(); },
        },
      ],
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REMOVE ENTRY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: prompt to remove the entry, doing prior appropriate checks
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  removeEntryPrompt: function () {
    var postified = this;

    //
    //                              IF NECESSARY, EXECUTE beforeRemove CALLBACK

    function confirmValidated () {
      if (postified.options.beforeRemove) postified.options.beforeRemove.call(postified, postified.entry, removeEntry)
      else removeEntry();
    };

    //
    //                              PROCEED TO REMOVAL

    function removeEntry () {
      postified.entry._deleted = true;
      postified.options.save(postified.entry, function () {
        postified.hasChanges = false; // set changed status to false
        postified.closeWithoutPrompt();
      });
    };

    //
    //                              SHOW REMOVAL CONFIRMATION DIALOG

    var confirmRemoveText = $$.result.call(postified, postified.options.removeEntryWarningContent, postified.entry) || "Remove this entry in the database?";

    // confirm prompting some validation text
    if (_.isFunction(postified.options.removeEntryWarningValidationPrompt)) dialogify.input({
      comment: confirmRemoveText,
      type: "danger",
      input: { inputifyType: "normal", },
      ok: function (enteredText) {
        // only continue if entered text is appropriate validation
        if (postified.options.removeEntryWarningValidationPrompt.call(postified, enteredText, postified.entry)) confirmValidated();
      },
    })
    // confirm with simple ok/cancel buttons
    else uify.confirm({
      content: confirmRemoveText,
      type: "danger",
      ok: confirmValidated,
    });

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CLOSE POSTIFY WINDOW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: close this postify window
    ARGUMENTS: ( ø )
    RETURN: <false|undefined> « will return false if window shouldn't be closed right away »
  */
  close: function () {
    var postified = this;

    if (postified.hasChanges || postified.$window.find(".invalid").length) {
      uify.confirm({
        type: "danger",
        content: "Close window and lose the modifications you made?",
        ok: function () { postified.closeWithoutPrompt(); },
      });
      return false;
    }
    else postified.closeWithoutPrompt();

  },

  /**
    DESCRIPTION: FINAL CLOSE - use this one always to close window and don't act directly on windified, because otherwise it will skip "closed" event
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  closeWithoutPrompt: function () {
    var postified = this;

    postified.windified.destroy();
    if (postified.options.closed) postified.options.closed.call(postified);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  LIST CHANGES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display list of changes made to the edited entry
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  listChanges: function () {
    var postified = this;

    uify.differences.alert({
      before: postified.entry,
      after: postified.resultEntry,
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT ENTRY RAW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: edit the entry in raw view
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  editRaw: function () {
    var postified = this;

    dialogify.json({
      title: "edit entry raw",
      value: postified.resultEntry,
      ok: function (result) {
        postified.inputified.set(result);
        postified.spacified.visit(0);
        postified.spacified.removeAfter();
      },
      cancel: function (result) {
        var postifiedInputifiedValue = JSON.parse(JSON.stringify( postified.inputified.get() ));
        if (!_.isEqual(result, postifiedInputifiedValue) && !confirm("Discard modifications?")) return false;
      },
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET ENTRY ID
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: propose to copy entry id
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  getId: function () {
    var postified = this;
    prompt("entry id", postified.resultEntry._id);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SHOW ADVANCED INPUTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: toggle display of advanced inputs
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  toggledAdvanvedInputsDisplay: function () {
    var postified = this;

    // initialize if not yet done
    if (_.isUndefined(postified.advancedInputDisplay)) postified.advancedInputDisplay = true;

    // toggle inputs visibility
    if (postified.advancedInputDisplay) postified.$window.addClass("postify-advanced_inputs_force_invert_visibility")
    else postified.$window.removeClass("postify-advanced_inputs_force_invert_visibility");

    // toggle inputs visibility status
    postified.advancedInputDisplay = !postified.advancedInputDisplay;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
