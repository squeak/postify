var _ = require("underscore");
var $$ = require("squeak");

/**
  DESCRIPTION: list of global shortcuts for postify
  TYPE: ({
    [shortcut string]: {
      !description: <string> « a description of what this shortcut is doing »,
      !action: <function(
        !event,
        ?windified « currently active postify window »,
      ):<void>}>,
    },
  })
*/
module.exports = {
  "shift + s": {
    keycomboId: "postify--save_entry",
    description: "save entry currently under edition",
    action: function (event, windified) {
      if (windified && windified.postified) windified.postified.saveEntryPrompt();
    },
  },
  "shift + r": {
    keycomboId: "postify--raw_edition",
    description: "raw edition of the entry currently under edition",
    action: function (event, windified) {
      if (windified && windified.postified) windified.postified.editRaw();
    },
  },
  "shift + 8": {
    keycomboId: "postify--get_id",
    description: "get id of the entry currently under edition",
    action: function (event, windified) {
      if (windified && windified.postified) windified.postified.getId();
    },
  },
  "shift + c": {
    keycomboId: "postify--show_changes",
    description: "show what as been changed in the currently edited entry, since last saved version",
    action: function (event, windified) {
      if (windified && windified.postified) windified.postified.listChanges();
    },
  },
  "shift + =": {
    keycomboId: "postify--display_advanced_inputs",
    description: "toggle display of advanced inputs",
    action: function (event, windified) {
      if (windified && windified.postified) windified.postified.toggledAdvanvedInputsDisplay();
    },
  },
};
