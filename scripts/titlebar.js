var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/array");

var titlebar = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  BUTTONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  getButtons: function (postified) {

    //
    //                              DEFAULT BUTTONS

    var defaultButtons = [
      {
        name: "advanced",
        title: "advanced",
        icomoon: "menu",
        clickMenu: {
          buttons: [
            {
              title: "see <u>c</u>hanges since saved version\n(shift + c)", // shift + c shortcut is handled globally
              icomoon: "qrcode",
              key: "c",
              click: postified.listChanges,
            },
            {
              title: "edit entry <u>r</u>aw\n(shift + r)",
              icomoon: "code",
              key: "r",
              click: postified.editRaw,
            },
            {
              title: "get entry <u>i</u>d\n(shift + 8)",
              icomoon: "link3",
              key: "i",
              click: postified.getId,
            },
            {
              title: "toggle display of <u>a</u>dvanced inputs\n(shift + =)", // shift + = shortcut is handled globally
              icomoon: "3dglasses",
              key: "a",
              click: postified.toggledAdvanvedInputsDisplay,
            },
          ],
        },
      },
      {
        name: "remove",
        title: "remove entry",
        icomoon: "trash",
        condition: function () { return !postified.options.isNewEntry; },
        click: postified.removeEntryPrompt,
      },
      {
        name: "save",
        title: "save entry modifications\n(shift + s)",  // shift + s shortcut is handled globally
        icomoon: "floppy-disk",
        click: postified.saveEntryPrompt,
      },
    ];

    //
    //                              MAKE LIST OF ALL BUTTONS

    return $$.array.merge(postified.options.buttons, defaultButtons);

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  getTitle: function (postified, titleOption) {
    return (postified.options.isNewEntry ? "NEW | " : "") + $$.result(titleOption, postified.options.entry);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = titlebar;
