var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/color");
require("squeak/extension/dom");
var $ = require("yquerj");
var inputify = require("inputify");
var uify = require("uify");
var methods = require("./methods");
var titlebar = require("./titlebar");
var shortcuts = require("./shortcuts");
var windify = require("windify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY INPUTIFY INPUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function displayInput (postified, $container, spacified) {

  //
  //                              CREATE HELP MESSAGE

  var helpMessage = $$.result.call(postified, postified.options.help);
  if (helpMessage) $container.div({
    class: "help-message",
    htmlSanitized: helpMessage,
  });

  //
  //                              CREATE INPUTIFIES

  var recursiveEntryOptions = {
    _recursiveOption: true,
    saveToParentOnChange: true,
    useCustomDialog: function (inputifyOpenEntryOptions) {
      return openSubEntry(this.name, inputifyOpenEntryOptions, spacified);
    },
  };

  postified.inputified = inputify({
    $container: $container,
    type: "object",
    object: { structure: postified.options.structure, },
    labelLayout: "hidden",
    defaultValue: postified.resultEntry,
    storageRecursive: {
      postified: postified,
      isNewEntry: postified.options.isNewEntry,
      options: {
        entry: recursiveEntryOptions,
      },
    },
    changed: function (result) {
      postified.defineValueInObject(result);
    },
    setCallback: function (result) {
      postified.defineValueInObject(result);
    },
  });

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  OPEN SUB-ENTRY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function openSubEntry (inputName, inputifyOpenEntryOptions, spacified) {

  // remove all spaces after the current one
  spacified.removeAfter();

  // create new space containing the entry edition content
  var spaceObject = spacified.create({
    name: !_.isUndefined(inputName) ? inputName : "<unknownKey>",
    contentMaker: inputifyOpenEntryOptions.contentMaker,
    spaceButtons: [
      // Removed the following, because it's not so useful, the left arrow is making almost the same thing, and this icon is confusing, making users think they saved the entry
      // {
      //   position: "left",
      //   icomoon: "floppy-disk",
      //   title: "validate changes of this subentry",
      //   click: function () {
      //     // inputifyOpenEntryOptions.save(); // not useful it's done automatically, because inputified.options.entry.saveToParentOnChange is set to true
      //     spacified.previous();
      //     spacified.removeAfter();
      //   },
      // },
      {
        position: "left",
        icomoon: "cross",
        title: "discard changes of this subentry",
        click: function () {
          var confirmed = inputifyOpenEntryOptions.discard();
          if (confirmed != false) {
            spacified.previous();
            spacified.removeAfter();
          };
        },
      },
    ],
  });

  // return elements required by inputify
  return {
    dialog: spaceObject,
    destroy: function () {
      spacified.previous(); // not perfect, if used programmatically when a subentry has been opened, will not return to appropriate space, but that's ok enough for now
      spacified.removeAfter();
    },
  };

};

function calculateOptions (options) {
  return $$.defaults(
    // default options
    {
      displayFab: "touchDevices",
      draggable: true,
      title: function (entry) { return entry._id; },
    },
    // passed options
    options,
    // options to recalculate at each refresh
    _.isFunction(options.postifyOptionsMaker) ? options.postifyOptionsMaker() : undefined
  );
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: edit an entry in a window with nice capabilities
  ARGUMENTS: ( postifyOptions <{

    //
    //                              MANDATORY SETTINGS

    !$container: <jqueryProcessable>@default=$("body") « in which dom element to display the edition window »,
    !entry: <couchEntry> « the entry to edit »,
    !structure: <inputify·options[]> «
      list of inputs to display to edit this entry
      all inputified of postify inputs will have an "isNewEntry" key set in inputified.storageRecursive defining if this input is part of creating a new entry or not, this is for example useful if you want to generate default values differently depending on the entry status
      the postified object will also be available in every inputified.storageRecursive.postified
    »,
    !save: <function(
      !entry <couchEntry> « the entry to put in the database »,
      !callback <function(entrySaved<couchEntry>)> «
        you should execute this callback on saving success
        it is important that you pass to this callback the saved entry
      »,
    )> « the method that will save the entry to the database »,
    ?postifyOptionsMaker: <function(ø):<postify·options>> « allow you to add any options that will be recalculated every time the view window is refreshed »,

    //
    //                              OPTIONAL SETTINGS

    ?title: <string> « the title to display in top bar »,
    ?subtitle: <string> « the subtitle to display in top bar »,
    ?icon: <string> « the icon to display in top bar »,
    ?image: <string> « an image instead of the icon, to display in top bar (if this is defined, and icon also, this will have priority) »,
    ?bodyColor: <string> « a color to give to the edition window body »,
    ?titleBarColor: <string> « a color to give to the edition window title bar »,

    ?created: <function(){@this=postified}> « callback ran when window has been created »,
    ?ok: <function(ø)> « executed when save has been done »,
    ?changed: <function(entry<couchEntry>){@this=postified}> « executed when entry is modified »,
    ?isNewEntry: <boolean> « pass true if the entry is not yet in pouch (changes a bit the look of postify) »,
    ?help: <string|function(){@this=postified}:<string>> « a help message to display on top of the window »,

    ?displayFab: <"all"|"touchDevices"|"none">@default="touchDevices" « when to display the save entries fab »,

    // CALLBACKS
    ?beforeSave: <function(
      entryModified <couchEntry>,
      entryBeforeModifs <couchEntry>,
      callback<function(ø):<void>>
    ){@this=postified}> « ran before saving an entry, to approve the saving, you must run the callback »,
    ?beforeRemove: <function(
      entryModified <couchEntry>,
      entryBeforeModifs <couchEntry>,
      callback<function(ø):<void>
    ){@this=postified}> « ran before removing an entry, to approve the removal, you must run the callback »,
    ?closed: <function(ø){@this=postified}> « executed when the window has been closed »,
    ?removeEntryWarningContent: <
      | string
      | function(entryBeforeModifs <couchEntry>){@this=<postified>}:<string>
    > « some customized contents to display in the remove entry dialog »,
    ?removeEntryWarningValidationPrompt: <function(
      enteredText <string|undefined>,
      entryBefore <couchEntry>
    ):<boolean>> «
      if this function is defined, instead of a simple confirm dialog, a prompt will be displayed,
      the user will have to input a text, and this function should return a boolean to validate or not the removal of this entry
    »

    ?draggable: <boolean>@default=true « if true, we possible to drag postify container by its top bar »,

    ?buttons: <uify·button.options[]> «
      custom buttons to add to postify window
      the click event of the buttons will be passed postified as context (will work also with clickSelect, clickUnselect, clickMenu.click and clickMenu.buttons[].click events)
    »,
    ?buttonsContext <any> « context passed to titlebar buttons (will be the this of click callbacks functions for those buttons) »,

    ?storage: <{ [string]: <any> }> « add anything here that you want to store in the postified object »

    //                              ¬
    //

  }> )
  RETURN: postified: <{
    isPostified: true,
    $container: <yquerjObject> « the jquery dom element of this postify window container »,
    $window: <yquerjObject> « the jquery dom element of this postify window »,
    entry: <couchEntry> « the original entry passed, before any modification »,
    options: potify·options,
    hasChanges: <boolean> « is true when resultEntry is different than the originally given one »,
    resultEntry: <couchEntry>,
    inputified: <inputified>,
    toolbarButtons: <uify.buttons·return>,
    windified: <windify·return> « the windified object for this postified window »,
    storage: <object> « anything you passed to postifyOptions.storage, or if you passed nothing, an empty object »,
    spacified: <uify.spaces·return> « the spacified object of this postify window »,
    ... see full list of windified methods
  }>;

*/
var postify = function (options) {

  //
  //                              HANDLE OPTIONS

  // make sure container is yquerj element
  options.$container = $(options.$container || "body");

  // check mandatory options
  $$.mandatory({
    $container: $$.isJqueryElement,
    save: _.isFunction, // TODO: could be more specific??
    entry: _.isObject,
  });

  options = calculateOptions(options);

  //
  //                              POSTIFIED OBJECT

  var postified = {
    isPostified: true,
    $container: options.$container,
    entry: options.entry,
    storage: options.storage || {},
    options: options,
    hasChanges: false,
    resultEntry: $$.clone(options.entry, 99),
    recalculateOptions: function () { postified.options = calculateOptions(postified.options); },
  };

  //
  //                              ATTACH METHODS

  _.each(methods, function (methodFunc, methodName) {
    postified[methodName] = _.bind(methodFunc, postified);
  });

  //
  //                              CREATE DOM ELEMENTS

  postified.windified = windify({

    $container: options.$container,
    class: "postify",
    close: function () { return postified.close(); },

    buttons: titlebar.getButtons(postified),
    buttonsContext: postified,

    windifyOptionsMaker: function (opts) {
      if (_.isFunction(postified.options.windifyOptionsMaker)) opts = postified.options.windifyOptionsMaker(opts) || {};
      return {
        title: titlebar.getTitle(postified, opts.title),
        subtitle: opts.subtitle,
        icon: opts.icon,
        image: opts.image,
        titleBarColor: opts.titleBarColor,
        bodyColor: opts.bodyColor ? $$.color.opacity(opts.bodyColor, 0.8) : undefined,
      };
    },

    // CONTENT IS NOT CREATED WITH contentMaker SO THAT postified.windified EXIST WHEN INPUTIFIED IS CREATED...
  });

  //
  //                              ATTACH POSTIFIED TO WINDIFIED FOR USAGE IN SHORTCUTS

  postified.windified.postified = postified;

  //
  //                              CREATE SPACES CONTAINER

  postified.spacified = uify.spaces({
    $target: postified.windified.$body,
    controlsPosition: "top",
    animate: true,
    fullPathBar: true,
    pathDelimitor: "♦",
  });

  // create first space containg the entry under edition
  postified.spacified.create({
    name: "{·}",
    contentMaker: function ($spaceContainer) {
      displayInput(postified, $spaceContainer, postified.spacified);
    },
  })

  //
  //                              TUNE $window A LITTLE BIT

  postified.$window = postified.windified.$window;

  // set newEntry class state
  if (options.isNewEntry) postified.$window.addClass("postify-new_entry")
  else postified.$window.removeClass("postify-new_entry");

  //
  //                              ADD SAVE FAB IF TOUCH SCREEN

  if (options.displayFab == "all" || (options.displayFab == "touchDevices" && $$.isTouchDevice())) uify.fab({
    $container: postified.$window,
    position: { right: "default", bottom: "default", },
    icomoon: "floppy-disk",
    click: postified.saveEntryPrompt,
  });

  //
  //                              RETURN POSTIFY OBJECT, AND ALSO SAVE IT TO IT'S CONTAINER

  postified.$window.data("postified", postified);

  // focus the first input in the window
  var $inputsInWindow = postified.$window.find(".inputify_container:not(.locked) > input, .inputify_container:not(.locked) > textarea");
  if ($inputsInWindow.length && !$$.isTouchDevice()) $$.dom.focus($inputsInWindow[0]);

  if (_.isFunction(options.created)) options.created.call(postified);
  return postified;

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE SHORTCUTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

windify.shortcuts.add(shortcuts);

/**
  DESCRIPTION: manipulate postify shortcuts
  TYPE: <see windify.shortcuts in "https://framagit.org/squeak/windify"> «
    postify.shortcuts is just an alias of windify.shortcuts, so they share the same list of shortcuts
  »
*/
postify.shortcuts = windify.shortcuts;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = postify;
