# postify

A library to edit a database entry (json object) in a graphical way..

For the full documentation, installation instructions... check [postify documentation page](https://squeak.eauchat.org/libs/postify/).
